package android.aranaj.crystalball;

public class Predictions {
    public static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[]{
            "Your wishes will come true.",
            "Your wishes will never come true!",
            "You are boring",
            "You suck",
            "You are terrible",
            "Juzet is on top"
    };
    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }
    public String getPredictions() {
        //Math.rand stuff
        int range = (answers.length - 1);
        int rand = (int)(Math.random() * range) + 1;
        return answers[rand];
    }
}

